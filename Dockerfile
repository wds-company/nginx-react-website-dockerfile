#########################################################
# Create run image
#########################################################
# production environment
FROM nginx:1.17.1-alpine
RUN rm /etc/nginx/conf.d/default.conf
COPY ./Files/nginx.conf /etc/nginx/conf.d
#EXPOSE 80